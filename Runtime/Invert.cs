﻿namespace UMNP.GamedevKit.BehaviourTree
{
    /// <summary>
    /// Inverts the state of the target node, pass if target is running.
    /// </summary>
    public class Invert : Node
    {
        private Node _target;

        public Invert(Node node) =>
            _target = node;

        public override void Clear()
        {
            _target.Clear();
            _target = null;
        }

        public override State Evaluate()
        {
            switch (_target.Evaluate())
            {
                case State.Success:
                    return State.Fail;
                case State.Fail:
                    return State.Success;
                case State.Running:
                    return State.Running;
                default:
                    return State.Fail;
            }
        }
    }
}
